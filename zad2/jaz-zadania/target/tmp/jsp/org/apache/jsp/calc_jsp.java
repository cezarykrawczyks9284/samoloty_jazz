package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import pl.edu.pjwstk.jaz.zadanie.*;

public final class calc_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n");
      out.write("   \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    ");
      pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe skorka = null;
      synchronized (session) {
        skorka = (pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe) _jspx_page_context.getAttribute("skorka", PageContext.SESSION_SCOPE);
        if (skorka == null){
          skorka = new pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe();
          _jspx_page_context.setAttribute("skorka", skorka, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("\t\t");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("skorka"), "wybranaSkorka", request.getParameter("kolorSkorki"), request, "kolorSkorki", false);
      out.write("\n");
      out.write("\t\t");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("skorka"), "pierwszaLiczba", request.getParameter("pierwszaLiczba"), request, "pierwszaLiczba", false);
      out.write("\n");
      out.write("\t\t");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("skorka"), "drugaLiczba", request.getParameter("drugaLiczba"), request, "drugaLiczba", false);
      out.write("\n");
      out.write("\t\t");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("skorka"), "dzialanie", request.getParameter("dzialanie"), request, "dzialanie", false);
      out.write("\n");
      out.write("    <body style=\"background-color:#");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe)_jspx_page_context.findAttribute("skorka")).getWybranaSkorka())));
      out.write(";\">\n");
      out.write(" \t\t\n");
      out.write(" \t\t");
 if(request.) out.println("test"); 
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe)_jspx_page_context.findAttribute("skorka")).getWybranaSkorka())));
      out.write("\n");
      out.write("\t\t\t<form method=\"POST\" >\n");
      out.write("\t\t\tPierwsza liczba: <br />\n");
      out.write("\t\t\t<input type=\"text\" value='");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe)_jspx_page_context.findAttribute("skorka")).getPierwszaLiczba())));
      out.write("' name=\"pierwszaLiczba\"/>\n");
      out.write("\t\t\t<br />\n");
      out.write("\t\t\tDruga liczba: \n");
      out.write("\t\t\t<br />\n");
      out.write("\t\t\t<input type=\"text\" value=");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe)_jspx_page_context.findAttribute("skorka")).getDrugaLiczba())));
      out.write(" name=\"drugaLiczba\"/>\n");
      out.write("\t\t\t<br />\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t<input type=\"radio\" name=\"dzialanie\" value=\"+\"/>+\n");
      out.write("\t\t\t<input type=\"radio\" name=\"dzialanie\" value=\"-\"/>-\n");
      out.write("\t\t\t<input type=\"radio\" name=\"dzialanie\" value=\"*\"/>*\n");
      out.write("\t\t\t<input type=\"radio\" name=\"dzialanie\" value=\"/\"/>/\n");
      out.write("\t\t\t<br />\n");
      out.write("\t\t\t<input type=\"submit\" name=\"wyslij\" value=\"ok\"/>\n");
      out.write("\t\t\t</form>\n");
      out.write(" \t\t\tWynik:\n");
      out.write("\t\t\tif(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${skorka.oblicz(skorka.dzialanie)}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(")\n");
      out.write("\t\t\tdziałanie: ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${skorka.dzialanie}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("\t\t\t<br>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
