package pl.edu.pjwstk.jaz.hellojaz.form;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String bParam = request.getParameter("b");
		out.println("Wartość parametru b to \""+bParam+"\"<br/>");
		
		String selectedHobby = "";
		if(request.getParameterValues("hobby") != null)
			for (String hobby : request.getParameterValues("hobby")) {
				selectedHobby += hobby + " ";
			}
		
		String firstName = request.getParameter("firstName");
		out.println("<html><body><h2>Your data</h2>" +
				"<p>First name: " + firstName + "<br />" +
				"<p>Surname: " + request.getParameter("surName") + "<br />" +
				"<p>Your hobby: " + selectedHobby + "<br /><br/>" +
				"<a href=\"hello\">hello</a>" +
				"</body></html>");
		out.close();
		
		HttpSession session = request.getSession();
		session.setAttribute("name", firstName);
	}

}
