package pl.edu.pjwstk.jaz.hellojaz;

/**
 * Nasz pierwszy JavaBean.
 * Jego pola odpowiadają polom w formularzu ze strony form.jsp
 */
public class PersonalData {

	private String name = "Grzegorz";
	private String surname = "Brzeczyszczykiewicz";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
}
