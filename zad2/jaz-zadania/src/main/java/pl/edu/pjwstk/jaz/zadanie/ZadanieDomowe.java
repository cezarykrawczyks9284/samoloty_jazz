package pl.edu.pjwstk.jaz.zadanie;

public class ZadanieDomowe {
	private String wybranaSkorka="fff";
	private int pierwszaLiczba=0;
	private int drugaLiczba=0;
	private String dzialanie="+";
	private int licznikSkorki1=0;
	private int licznikSkorki2=0;
	private int licznikSkorki3=0;
	private int licznikSkorki4=0;
	
	public String getWybranaSkorka() {
		return wybranaSkorka;
	}
	public void setWybranaSkorka(String wybranaSkorka) {
		this.wybranaSkorka = wybranaSkorka;
	}
	public int getPierwszaLiczba() {
		return pierwszaLiczba;
	}
	public void setPierwszaLiczba(int pierwszaLiczba) {
		this.pierwszaLiczba = pierwszaLiczba;
	}
	public int getDrugaLiczba() {
		return drugaLiczba;
	}
	public void setDrugaLiczba(int drugaLiczba) {
		this.drugaLiczba = drugaLiczba;
	}
	public int getLicznikSkorki1() {
		return licznikSkorki1;
	}
	public void setLicznikSkorki1() {
		this.licznikSkorki1 += 1;
	}
	public int getLicznikSkorki2() {
		return licznikSkorki2;
	}
	public void setLicznikSkorki2() {
		this.licznikSkorki2 +=1;
	}
	public int getLicznikSkorki3() {
		return licznikSkorki3;
	}
	public void setLicznikSkorki3() {
		this.licznikSkorki3+=1;
	}
	public int getLicznikSkorki4() {
		return licznikSkorki4;
	}
	public void setLicznikSkorki4() {
		this.licznikSkorki4 +=1;
	}
	public String getDzialanie() {
		return dzialanie;
	}
	public void setDzialanie(String dzialanie) {
		this.dzialanie = dzialanie;
	}
	public float oblicz(String dzialanie){
		if(dzialanie.equals("+")){
			return (this.pierwszaLiczba+this.drugaLiczba);
		}else if(dzialanie.equals("-")){
			return (this.pierwszaLiczba-this.drugaLiczba);
		}else if(dzialanie.equals("*")){
			return (this.pierwszaLiczba*this.drugaLiczba);
		}else if(dzialanie.equals("/")){
			return (this.pierwszaLiczba/this.drugaLiczba);
		}else{
			return 0;
		}
	}
}
