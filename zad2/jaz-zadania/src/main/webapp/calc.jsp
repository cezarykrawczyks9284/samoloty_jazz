<%@page import="pl.edu.pjwstk.jaz.zadanie.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <jsp:useBean id="skorka" 
 			class="pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe"
 			scope="session"/>
		<jsp:setProperty property="wybranaSkorka" name="skorka" param="kolorSkorki"/>
		<jsp:setProperty property="pierwszaLiczba" name="skorka" param="pierwszaLiczba"/>
		<jsp:setProperty property="drugaLiczba" name="skorka" param="drugaLiczba"/>
		<jsp:setProperty property="dzialanie" name="skorka" param="dzialanie"/>
    <body style="background-color:#<jsp:getProperty property="wybranaSkorka" name="skorka"/>;">
 		
 			<% if(skorka.getWybranaSkorka()=="fff"){ %>
 				Wybierz najpierw kolor skórki!<br />
 				<br /><br />
        <hr />
        <a href="start.jsp">Start</a> | <a href="#">Oblicz</a> | <a href="stats.jsp">Statystyki</a>
			<% }else { 
				if(skorka.getWybranaSkorka().equals("A7A7A7")){skorka.setLicznikSkorki1();}
				else if(skorka.getWybranaSkorka().equals("0363A3")){skorka.setLicznikSkorki2();}
				else if(skorka.getWybranaSkorka().equals("0C8C4B")){skorka.setLicznikSkorki3();}
				else if(skorka.getWybranaSkorka().equals("C50A2A")){skorka.setLicznikSkorki4();}
				
				%>
				<form method="POST" >
				Pierwsza liczba: <br />
				<input type="text" value='<jsp:getProperty property="pierwszaLiczba" name="skorka"/>' name="pierwszaLiczba"/>
				<br />
				Druga liczba: 
				<br />
				<input type="text" value=<jsp:getProperty property="drugaLiczba" name="skorka"/> name="drugaLiczba"/>
				<br />
				
				<input type="radio" name="dzialanie" value="+"/>+
				<input type="radio" name="dzialanie" value="-"/>-
				<input type="radio" name="dzialanie" value="*"/>*
				<input type="radio" name="dzialanie" value="/"/>/
				<br />
				<input type="submit" name="wyslij" value="ok"/>
				</form>
	 			Wynik:
				${skorka.oblicz(skorka.dzialanie)}
				działanie: ${skorka.dzialanie}
				<br>
				<br /><br />
        <hr />
        <a href="start.jsp">Start</a> | <a href="#">Oblicz</a> | <a href="stats.jsp">Statystyki</a>
			<% } %>
    </body>
</html>
