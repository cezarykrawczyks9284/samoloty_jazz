<%@page import="pl.edu.pjwstk.jaz.zadanie.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Statystyki</title>
    </head>
    <jsp:useBean id="skorka" 
 			class="pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe"
 			scope="session"/>
		<body style="background-color:#<jsp:getProperty property="wybranaSkorka" name="skorka"/>;" >
 		
 			<table>
 			<tr>
 				<th>Lp.</th>
 				<th>Kolor</th>
 				<th>Ilość</th>
 			</tr>
 			<tr>
 				<td>1</td>
 				<td>szara</td>
 				<td>${skorka.getLicznikSkorki1()}</td>
 			</tr>
 			<tr>
 				<td>2</td>
 				<td>niebieska</td>
 				<td>${skorka.getLicznikSkorki2()}</td>
 			</tr>
 			<tr>
 				<td>3</td>
 				<td>zielona</td>
 				<td>${skorka.getLicznikSkorki3()}</td>
 			</tr>
 			<tr>
 				<td>4</td>
 				<td>czerwona</td>
 				<td>${skorka.getLicznikSkorki4()}</td>
 			</tr>
 			</table>
 			<br /><br />
        <hr />
        <a href="start.jsp">Start</a> | <a href="calc.jsp">Oblicz</a> | <a href="#">Statystyki</a>
    </body>
</html>
