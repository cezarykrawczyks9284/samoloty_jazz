<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

           <h1>Hello World!</h1>
        
        <%-- expressions --%>
        2 + 2 = <%= 2 + 2 %><br>
        <%= "Ala ma kota" %><br>
        <%= new java.util.Date()%><br>
				Serwer: <%= application.getServerInfo() %><br><br>
        
        <%-- skryptlety --%>
        <% out.println("Ala ma kota"); %><br>
        <% for(int i=1; i<10; i++) {
        	 for(int j=0; j<(10-i)/2; j++) { %>
        	 &nbsp;
        <% } for (int j=0;j<i;j++) { %>
        *
        <% } %>
        <br>
        <% } %><br>
        
        Imie podane w formularzu: <%= session.getAttribute("name") %><br>
        
        <%-- dyrektywy --%>
        <%@page import="java.util.*" %>
        <%= new Date() %>
        
        <%@include file="baner.txt" %><br>
        
        <%-- deklaracja pól i metod klasy --%>
        <%! Date data = new Date(); %>
        Strona działa od <%= data %><br>
        
        <%! private String dajGlos() {
        	return "ajajaj!";
        }
        %>
        <%= dajGlos() %><br>
        
        <%! private int licznik=1; %>
        <p style="float:right; color:green; font-size:small;">        
        Licznik odwiedzin strony: <%= licznik++ %>
        </p>
        
    </body>
</html>
