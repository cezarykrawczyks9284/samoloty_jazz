<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

		<jsp:useBean id="person" 
 			class="pl.edu.pjwstk.jaz.hellojaz.PersonalData"
 			scope="session"/>

		<jsp:setProperty property="*" name="person"/>

		<p>Drogi użytkowniku, oto twoje dane:</p>
    imię: <jsp:getProperty property="name" name="person"/><br>
 		nazwisko: <jsp:getProperty property="surname" name="person"/><br>    
    
    </body>
</html>
