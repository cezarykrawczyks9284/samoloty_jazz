<%@page import="pl.edu.pjwstk.jaz.zadanie.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zadanie domowe nr 2</title>
    </head>
    <jsp:useBean id="skorka" 
 			class="pl.edu.pjwstk.jaz.zadanie.ZadanieDomowe"
 			scope="session"/>
    <body style="background-color:#<jsp:getProperty property="wybranaSkorka" name="skorka"/>;">
			
        <h1>Witaj drogi użytkowniku!</h1>
        <p> Wybierz jakiej skórki chcesz używać : </p>
        
        <form method="POST" action="calc.jsp">
        
	        <select name="kolorSkorki">
	        <option value="A7A7A7">szara</option>
	        <option value="0363A3">niebieska</option>
	        <option value="0C8C4B">zielona</option>
	        <option value="C50A2A">czerwona</option>
	        </select>
        	<input value="ok" name="ok" type="submit"/>
        </form>
        <br /><br />
        <hr />
        <a href="start.jsp">Start</a> | <a href="calc.jsp">Oblicz</a> | <a href="stats.jsp">Statystyki</a>
    </body>
</html>
