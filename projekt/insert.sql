
SET DATESTYLE TO 'European,German';
SET client_encoding='utf-8';

insert into stadion(nazwa,pojemnosc)  values(' Camp Nou','98260');
insert into stadion(nazwa,pojemnosc)  values('Santiago Bernabéu','81254');
insert into stadion(nazwa,pojemnosc)  values('Signal Iduna Park','81264');
insert into stadion(nazwa,pojemnosc)  values('PGE Arena Gdańsk','43578');
insert into stadion(nazwa,pojemnosc)  values('Stadion Wojska Polskiego','33254');
insert into stadion(nazwa,pojemnosc)  values('Old Trafford','75797');

insert into liga(nazwa) values ('Primera Division'); 	
insert into liga(nazwa) values ('Ekstraklasa');
insert into liga(nazwa) values ('Premier League');
insert into liga(nazwa) values ('Bundesliga');


insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('FC Barcelona','Primera Division','1899-11-28',1,'49','57','19');
insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('Real Madryt','Primera Division','1902-07-05',2,'33','41','17');
insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('Manchester United','Premier League','1909-01-10',6,'56','53','25');
insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('Lechia Gdańsk','Ekstraklasa','1912-05-30',4,'23','14','5');
insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('Legia Warszawa','Ekstraklasa','1899-04-15',5,'32','32','14');
insert into klub(nazwa, liga, data_zalozenia, id_stadion, punkty, bramki_zdobyte, bramki_stracone)
   values('Borussia Dortmund','Bundesliga','1909-12-05',3,'55','44','7');


insert into trenerzy(id_klubu,imie ,nazwisko ) values (1,'Tito', 'Vilanova');
insert into trenerzy(id_klubu,imie ,nazwisko ) values (2,'José','Mourinho'); 
insert into trenerzy(id_klubu,imie ,nazwisko ) values (6,'Hermann','Eppenhoff');
insert into trenerzy(id_klubu,imie ,nazwisko ) values (5,'Arnold','Boczek');
insert into trenerzy(id_klubu,imie ,nazwisko ) values (4,'Wiesław','Dzban');
insert into trenerzy(id_klubu,imie ,nazwisko ) values (3,'David','Cameron');



insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('80012557481','Hernández','Xavi','33','Pomocnik','170','69','1980-01-25','Hiszpania','6',1);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('84051134312','Andrés','Iniesta','29','Pomocnik','176','64','1984-05-11','Hiszpania','8',1);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('78041354461','Carles','Puyol','35','Obrońca','178','80','1979-04-13','Hiszpania','5',1);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('81013457561','Iker','Casillas','34','Bramkarz','177','79','1981-01-23','Hiszpania','7',2);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('86061157481','Cristaiano','Ronaldo','37','Pomocnik','176','65','1986-06-11','Portugalia','11',2);

insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('82131231121','Janusz','Gol','33','Pomocnik','172','69','1986-06-11','Polska','23',5);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('85213213281','Inaki','Astiz','30','Obrońca','166','62','1985-06-21','Hiszpania','14',5);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('86061157481','Daniel','Nowak','37','Obrońca','186','70','1986-02-14','Polska','7',5);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('21312312323','Andrzej','Kowalski','37','Bramkarz','172','65','1986-06-11','Polska','2',5);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('82132131481','Cristaiano','Fernando','25','Bramkarz','172','68','1984-01-17','Portugalia','2',2);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('86069777481','Ronaldo','Biegardo','25','Napastnik','166','60','1986-06-11','Portugalia','23',1);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('86867857481','Zbigniew','Trąba','37','Napastnik','176','68','1986-07-15','Polska','11',1);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('86111777481','Jan','Nowak','27','Obrońca','168','69','1986-03-19','Polska','17',2);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('89999857481','Leo','Messi','27','Napastnik','176','69','1986-07-15','Polska','11',6);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('89999857481','Kuba','Błaszczykowski','27','Pomocnik','182','59','1989-01-15','Polska','15',4);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('89999851111','Kuba','Błaszczyk','27','Napastnik','183','69','1989-01-15','Polska','21',4);
insert into zawodnicy (pesel ,imie ,nazwisko ,wiek ,pozycja ,wzrost ,waga ,data_urodzenia ,narodowosc ,nr_koszulki ,id_klub) values ('84999857441','Jakub','Piszczek','23','Obrońca','172','59','1984-01-25','Polska','25',4);


insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (1,2,'2013-01-01','90123','2','1');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (2,1,'2013-02-05','50155','2','2');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (1,3,'2013-03-01','23523','0','0');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (3,1,'2013-04-05','56155','5','0');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (4,5,'2013-05-01','15911','1','1');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (5,4,'2013-06-05','20155','1','1');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (1,6,'2013-07-01','17876','6','0');
insert into mecze(id_gospodarz,id_gosc,data_meczu,liczba_widzow,gole_gosp,gole_gosc) values (6,4,'2013-08-05','23165','2','1');


insert into gole(id_mecz,id_zaw) values (1,4);
insert into gole(id_mecz,id_zaw) values (1,2);
insert into gole(id_mecz,id_zaw) values (1,1);
insert into gole(id_mecz,id_zaw) values (2,2);
insert into gole(id_mecz,id_zaw) values (2,3);
insert into gole(id_mecz,id_zaw) values (2,1);
insert into gole(id_mecz,id_zaw) values (2,5);


insert into gole(id_mecz,id_zaw) values (4,1);
insert into gole(id_mecz,id_zaw) values (4,1);
insert into gole(id_mecz,id_zaw) values (4,2);
insert into gole(id_mecz,id_zaw) values (4,3);
insert into gole(id_mecz,id_zaw) values (4,1);

insert into gole(id_mecz,id_zaw) values (5,6);
insert into gole(id_mecz,id_zaw) values (5,15);

insert into gole(id_mecz,id_zaw) values (6,15);
insert into gole(id_mecz,id_zaw) values (6,6);

insert into gole(id_mecz,id_zaw) values (7,1);
insert into gole(id_mecz,id_zaw) values (7,2);
insert into gole(id_mecz,id_zaw) values (7,1);
insert into gole(id_mecz,id_zaw) values (7,1);
insert into gole(id_mecz,id_zaw) values (7,2);
insert into gole(id_mecz,id_zaw) values (7,1);

insert into gole(id_mecz,id_zaw) values (8,16);
insert into gole(id_mecz,id_zaw) values (8,16);
insert into gole(id_mecz,id_zaw) values (8,17);



insert into kolejka(id_mecz) values (1);
insert into kolejka(id_mecz) values (2);
insert into kolejka(id_mecz) values (3);
insert into kolejka(id_mecz) values (4);
insert into kolejka(id_mecz) values (5);
insert into kolejka(id_mecz) values (6);
insert into kolejka(id_mecz) values (7);
insert into kolejka(id_mecz) values (8);
