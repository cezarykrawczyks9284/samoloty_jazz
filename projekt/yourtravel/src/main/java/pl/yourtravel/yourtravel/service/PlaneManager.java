package pl.yourtravel.yourtravel.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.yourtravel.yourtravel.domain.Plane;


@Stateless
public class PlaneManager {

	@PersistenceContext
	EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Plane> getAllPlane() {
		return em.createNamedQuery("plane.available").getResultList();
	}

}
