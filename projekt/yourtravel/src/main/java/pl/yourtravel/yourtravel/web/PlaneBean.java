package pl.yourtravel.yourtravel.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import pl.yourtravel.yourtravel.domain.Plane;
import pl.yourtravel.yourtravel.service.PlaneManager;

@SessionScoped
@Named("planeBean")
public class PlaneBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Plane plane = new Plane();
	private ListDataModel<Plane> planes = new ListDataModel<Plane>();
	

	@Inject
	private PlaneManager pm;

	public Plane getPlane() {
		return plane;
	}
	public void setPlane(Plane plane) {
		this.plane = plane;
	}
	
	// Actions
	public ListDataModel<Plane> getAllPlane() {
		planes.setWrappedData(pm.getAllPlane());
		return planes;
	}
}
