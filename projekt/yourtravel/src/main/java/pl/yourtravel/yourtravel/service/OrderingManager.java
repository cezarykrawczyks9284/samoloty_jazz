package pl.yourtravel.yourtravel.service;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.yourtravel.yourtravel.domain.Client;
import pl.yourtravel.yourtravel.domain.Plane;

@Stateless
public class OrderingManager {

	@PersistenceContext
	EntityManager em;

	public void buyTicket(Long clientId, Long planeId) {

		Client client = em.find(Client.class, clientId);
		Plane plane = em.find(Plane.class, planeId);
	/*	plane.setSold(true);*/
		client.getPlane().add(plane);
	}

	@SuppressWarnings("unchecked")
	public List<Plane> getAvailablePlane() {
		return em.createNamedQuery("plane.available").getResultList();
	}

	public void returnPlane(Client client, Plane plane) {

		client = em.find(Client.class, client.getId());
		plane = em.find(Plane.class, plane.getId());

		Plane toRemove = null;
		for (Plane aPlane : client.getPlane())
			if (aPlane.getId().compareTo(plane.getId()) == 0) {
				toRemove = aPlane;
				break;
			}
		if (toRemove != null)
			client.getPlane().remove(toRemove);
		
		/*plane.setSold(false);*/
	}
}
