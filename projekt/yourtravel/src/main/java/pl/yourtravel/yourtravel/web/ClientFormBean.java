package pl.yourtravel.yourtravel.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;


import pl.yourtravel.yourtravel.domain.Client;
import pl.yourtravel.yourtravel.domain.Plane;
import pl.yourtravel.yourtravel.service.ClientManager;
import pl.yourtravel.yourtravel.service.OrderingManager;


@SessionScoped
@Named("clientBean")
public class ClientFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Client client = new Client();
	private ListDataModel<Client> clients = new ListDataModel<Client>();
	
	private Client clientToShow = new Client();
	private ListDataModel<Plane> ownedPlane = new ListDataModel<Plane>();


	@Inject
	private ClientManager pm;
	
	@Inject
	private OrderingManager om;

	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	public ListDataModel<Client> getAllClients() {
		clients.setWrappedData(pm.getAllClients());
		return clients;
	}
	
	public ListDataModel<Plane> getOwnedPlane() {
		ownedPlane.setWrappedData(pm.getOwnedPlane(clientToShow));
		return ownedPlane;
	}
	
	// Actions
	public String addClient() {
		pm.addClient(client);
		return "showClients";
	}

	public String deleteClient() {
		Client clientToDelete = clients.getRowData();
		pm.deleteClient(clientToDelete);
		return null;
	}
	
	public String showDetails() {
		clientToShow = clients.getRowData();
		return "details";
	}
	
	public String returnPlane(){
		Plane planeToReturn = ownedPlane.getRowData();
		om.returnPlane(clientToShow, planeToReturn);
		return null;
	}
}
