package pl.yourtravel.yourtravel.web;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.yourtravel.yourtravel.domain.Client;
import pl.yourtravel.yourtravel.domain.Plane;
import pl.yourtravel.yourtravel.service.ClientManager;
import pl.yourtravel.yourtravel.service.OrderingManager;

@SessionScoped
@Named("orderBean")
public class OrderFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private OrderingManager om;
	@Inject
	private ClientManager pm;

	private Long planeId;
	private Long clientId;
	
	public Long getplaneId() {
		return planeId;
	}
	public void setplaneId(Long planeId) {
		this.planeId = planeId;
	}
	public Long getClientId() {
		return clientId;
	}
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public List<Plane> getAvailablePlane() {
		return om.getAvailablePlane();
	}

	public List<Client> getAllClients() {
		return pm.getAllClients();
	}

	public String buyTicket() {
		om.buyTicket(clientId, planeId);
		return "showClients";
	}
}
