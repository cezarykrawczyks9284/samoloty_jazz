package pl.yourtravel.yourtravel.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import pl.yourtravel.yourtravel.domain.Client;
import pl.yourtravel.yourtravel.domain.Plane;


@Stateless
public class ClientManager {

	@PersistenceContext
	EntityManager em;

	public void addClient(Client client) {
		client.setId(null);
		em.persist(client);
	}

	public void deleteClient(Client client) {
		client = em.find(Client.class, client.getId());
		em.remove(client);
	}

	@SuppressWarnings("unchecked")
	public List<Client> getAllClients() {
		return em.createNamedQuery("client.all").getResultList();
	}

	public List<Plane> getOwnedPlane(Client client) {
		if(client.getId()!=null)
			client = em.find(Client.class, client.getId());
		// lazy loading here - try this code without this (shallow) copying
		List<Plane> planes = new ArrayList<Plane>(client.getPlane());
		
		return planes;
	}

}
