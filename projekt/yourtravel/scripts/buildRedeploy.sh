#!/bin/sh

echo "************ UNDEPLOYING *******************"
asadmin undeploy yourtravel
echo "************ BUILDING **********************"
mvn package
echo "************ DEPLOYING *********************"
asadmin deploy target/yourtravel.war
